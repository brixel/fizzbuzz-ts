/*
Your task is to process a sequence of integer [1, 4, 6, 3, 2] numbers to determine the following statistics:
minimum value
maximum value
number of elements in the sequence
average value
*/

describe('Sequence', () => {

  it.each([
    // given
    [0, [0]],
    [1, [1]],
    [0, [0, 1]],
    [0, [1, 0]], 
    [0, [1, 0, 3]],
  ])(
    `should return '%s' if year %s is given`,
    (expectedResult:number, givenInput:number[]) => {
    // when
    const actualResult = minimumValue(givenInput);
    // then
    expect(actualResult).toBe(expectedResult);
  });

});

function minimumValue(givenInput): number {
  let result = givenInput[givenInput.length -1];
  
  let endValue = givenInput.length -1;
  let tmpValue = endValue;
  
  while (tmpValue >= 0) {
    result = result < givenInput[tmpValue] ? result : givenInput[tmpValue];
    tmpValue--;    
  }
  
  return result;
  //
  // if (givenInput.length === 1) return givenInput[0];
  // if (givenInput.length === 2) return givenInput[1] < givenInput[0] ? givenInput[1] : givenInput[0]
  // if (givenInput.length === 3) {
  //   let tmpResult = givenInput[1] < givenInput[0] ? givenInput[1] : givenInput[0];
  //   return tmpResult < givenInput[2] ? tmpResult : givenInput[2];
  // }
}

/*
  function minimumValue(givenInput:number[]) {
    return givenInput.reduce((prevValue, currValue) => {
      return 
  });
}
*/