import {fizzbuzz} from "../src/fizzbuzz";

describe('fizzbuzz', () => {

    it.each([
        ['1', 1],
        ['2', 2],
        ['fizz', 3],
        ['buzz', 5],
        ['fizz', 6],
        ['fizz', 9],
        ['buzz', 10],
        ['fizzbuzz', 15],
        ['16', 16],
    ])(
        `should return '%s' if %i is given`,
        (expectedResult, givenInput) => {
        // when
        const actualResult = fizzbuzz(givenInput);
        // then
        expect(actualResult).toBe(expectedResult);
    }) 
})