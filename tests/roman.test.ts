// 1 to I
// 2 to II
// 3 to III
// 4 to IV

describe('Roman numeral', () => {

  it.each([
    ['I', 1],
    ['II', 2],
    ['III', 3],
    ['IV', 4]
  ])(
      `should return '%s' if number %i is given`,
      (expectedResult, givenInput) => {
      // when
      const actualResult = convertNumeral(givenInput);
      // then
      expect(actualResult).toBe(expectedResult);
  });

});

function convertNumeral(givenInput:number) {
  return 'I' * givenInput;
  if (givenInput === 1) return 'I';
  if (givenInput === 2) return 'II';
  if (givenInput === 3) return 'III';
  return 'IV';
}