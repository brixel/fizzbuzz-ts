/* Leap year Write a function that returns true or false depending on
whether its input integer is a leap year or not. A leap year is divisible
by 4, but is not otherwise divisible by 100 unless it is also divisible by 400.
2001 is a typical common year, 1996 is a typical leap year, 1900 is an atypical
common year, 2000 is an atypical leap year
 */

describe('leap year', () => {
  it.each([
      [true, 1996],
      [false, 1997],
      [false, 1999],
      [false, 1900],
  ])(
      `should return '%s' if year %i is given`,
      (expectedResult, givenInput) => {
      // when
      const actualResult = leapYear(givenInput);
      // then
      expect(actualResult).toBe(expectedResult);
  });

});

function leapYear(givenInput:number) {
  if (givenInput %4 !== 0 || givenInput %100 === 0) return false;
  return true;
}
