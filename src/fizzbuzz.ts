export function fizzbuzz(givenInput: number) {
    if (givenInput%5 === 0 && givenInput%3 === 0) {
        return 'fizzbuzz';
    }

    if (givenInput%5 ===0 ) {
        return 'buzz';
    }
    if (givenInput%3 === 0) {
        return 'fizz';
    }
    return givenInput.toString();
}